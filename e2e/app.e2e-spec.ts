import { OfficialProjectPage } from './app.po';

describe('official-project App', function() {
  let page: OfficialProjectPage;

  beforeEach(() => {
    page = new OfficialProjectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { UserNameEditorComponent } from './components/USER-NAME-EDITOR.COMPONENT';
import { UserFormComponent } from './components/USER-NAME-EDITOR.COMPONENT';

@NgModule({
  declarations: [
    AppComponent,
    UserNameEditorComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

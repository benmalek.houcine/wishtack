import {Component} from '@angular/core';

@Component({
	selector: 'wt-user-name-editor',
	template: `
		<wt-user-form [user]="user" (onReset)="onReset()"></wt-user-form>					<br><br>
		<wt-user [user]="user"></wt-user>
	`
})
export class UserNameEditorComponent {

	user = {
		name: 'Foo'
	};

	onReset() {
		this.user = {
			name: null
		};
	}

}

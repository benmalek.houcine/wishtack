import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { UserComponent } from './components/USER.COMPONENT';
import { UserFormComponent } from './components/USER-FORM.COMPONENT';
import { UserNameEditorComponent } from './components/USER-NAME-EDITOR.COMPONENT';

@NgModule({
  declarations: [
    AppComponent,
    UserNameEditorComponent,
    UserFormComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
